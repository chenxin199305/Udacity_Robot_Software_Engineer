#!/bin/sh
export PARENT_DIR=$(cd $(dirname $0); cd .. ; pwd)
export CURRENT_DIR=$(cd $(dirname $0); pwd)
export TURTLEBOT_GAZEBO_WORLD_FILE=$CURRENT_DIR/empty_world.world;

set -e pipefail

xterm -e "source /opt/ros/kinetic/setup.bash; roscore" &
sleep 5

xterm -e "roslaunch turtlebot_gazebo turtlebot_world.launch" &
sleep 10

xterm -e "rviz" &
sleep 5
