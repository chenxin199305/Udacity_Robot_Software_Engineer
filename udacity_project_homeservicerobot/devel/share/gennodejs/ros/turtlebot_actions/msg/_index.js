
"use strict";

let FindFiducialFeedback = require('./FindFiducialFeedback.js');
let TurtlebotMoveFeedback = require('./TurtlebotMoveFeedback.js');
let FindFiducialActionResult = require('./FindFiducialActionResult.js');
let FindFiducialResult = require('./FindFiducialResult.js');
let TurtlebotMoveActionResult = require('./TurtlebotMoveActionResult.js');
let TurtlebotMoveGoal = require('./TurtlebotMoveGoal.js');
let FindFiducialActionFeedback = require('./FindFiducialActionFeedback.js');
let FindFiducialActionGoal = require('./FindFiducialActionGoal.js');
let FindFiducialAction = require('./FindFiducialAction.js');
let TurtlebotMoveResult = require('./TurtlebotMoveResult.js');
let TurtlebotMoveAction = require('./TurtlebotMoveAction.js');
let TurtlebotMoveActionFeedback = require('./TurtlebotMoveActionFeedback.js');
let TurtlebotMoveActionGoal = require('./TurtlebotMoveActionGoal.js');
let FindFiducialGoal = require('./FindFiducialGoal.js');

module.exports = {
  FindFiducialFeedback: FindFiducialFeedback,
  TurtlebotMoveFeedback: TurtlebotMoveFeedback,
  FindFiducialActionResult: FindFiducialActionResult,
  FindFiducialResult: FindFiducialResult,
  TurtlebotMoveActionResult: TurtlebotMoveActionResult,
  TurtlebotMoveGoal: TurtlebotMoveGoal,
  FindFiducialActionFeedback: FindFiducialActionFeedback,
  FindFiducialActionGoal: FindFiducialActionGoal,
  FindFiducialAction: FindFiducialAction,
  TurtlebotMoveResult: TurtlebotMoveResult,
  TurtlebotMoveAction: TurtlebotMoveAction,
  TurtlebotMoveActionFeedback: TurtlebotMoveActionFeedback,
  TurtlebotMoveActionGoal: TurtlebotMoveActionGoal,
  FindFiducialGoal: FindFiducialGoal,
};
