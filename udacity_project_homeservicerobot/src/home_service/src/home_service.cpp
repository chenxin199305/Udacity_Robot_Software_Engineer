#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/Marker.h>

// Define a client for to send goal requests to the move_base server through a SimpleActionClient
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// Position Information
// 0: at origin; 1: to target
// 2: at target; 3: to origin
int current_state = 0;

// Note : 
//    the position is defined by (x, y, z)
//    the orientation is defined by Quartanian (x, y, z, w)
float origin[7] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
float target[7] = {5.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0};

int main(int argc, char** argv){

  ROS_INFO("home_service start.");

  // Initialize the home_service node
  ros::init(argc, argv, "home_service");
  
  // Marker :
  ros::NodeHandle n;
  ros::Rate r(1);
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);

  uint32_t shape = visualization_msgs::Marker::CUBE;

  // Move :
  MoveBaseClient ac("move_base", true);

  // Wait 5 sec for move_base action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  while (ros::ok())
  {
    // Marker :
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/map";
    marker.header.stamp = ros::Time::now();

    // Move :
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "/map";
    goal.target_pose.header.stamp = ros::Time::now();

    ROS_INFO("current_state = %i", current_state);

    switch (current_state)
    {
    case 0:
    {
      ROS_INFO("current_state : at origin");

      // Marker :
      marker.ns = "marker";
      marker.id = 0;
      marker.type = shape;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.position.x = target[0];
      marker.pose.position.y = target[1];
      marker.pose.position.z = target[2];
      marker.pose.orientation.x = target[3];
      marker.pose.orientation.y = target[4];
      marker.pose.orientation.z = target[5];
      marker.pose.orientation.w = target[6];
      marker.scale.x = 0.2;
      marker.scale.y = 0.2;
      marker.scale.z = 0.2;
      marker.color.r = 0.0f;
      marker.color.g = 1.0f;
      marker.color.b = 0.0f;
      marker.color.a = 1.0;
      marker.lifetime = ros::Duration();

      while (marker_pub.getNumSubscribers() < 1)
      {
        if (!ros::ok())
        {
          return 0;
        }
        ROS_WARN_ONCE("Please create a subscriber to the marker");
        sleep(1);
      }
      marker_pub.publish(marker);

      current_state = 1;

      break;
    }
    case 1:
    {
      ROS_INFO("current_state : to target");

      // Move :
      goal.target_pose.pose.position.x = target[0];
      goal.target_pose.pose.position.y = target[1];
      goal.target_pose.pose.position.z = target[2];
      goal.target_pose.pose.orientation.x = target[3];
      goal.target_pose.pose.orientation.y = target[4];
      goal.target_pose.pose.orientation.z = target[5];
      goal.target_pose.pose.orientation.w = target[6];

      ROS_INFO("goal = {position = {%f %f %f}, orientation = {%f %f %f %f}",
      goal.target_pose.pose.position.x,
      goal.target_pose.pose.position.y,
      goal.target_pose.pose.position.z,
      goal.target_pose.pose.orientation.x,
      goal.target_pose.pose.orientation.y,
      goal.target_pose.pose.orientation.z,
      goal.target_pose.pose.orientation.w);
        
      ac.sendGoal(goal);
      ac.waitForResult();
      ROS_INFO("check the result...");
      if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
      {
        ROS_INFO("the target has reached succesfully!!!");

        // Marker :
        marker.ns = "marker";
        marker.id = 0;
        marker.type = shape;
        marker.action = visualization_msgs::Marker::DELETE;
        marker.pose.position.x = target[0];
        marker.pose.position.y = target[1];
        marker.pose.position.z = target[2];
        marker.pose.orientation.x = target[3];
        marker.pose.orientation.y = target[4];
        marker.pose.orientation.z = target[5];
        marker.pose.orientation.w = target[6];
        marker.scale.x = 0.2;
        marker.scale.y = 0.2;
        marker.scale.z = 0.2;
        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;
        marker.lifetime = ros::Duration();

        while (marker_pub.getNumSubscribers() < 1)
        {
            if (!ros::ok())
            {
            return 0;
            }
            ROS_WARN_ONCE("Please create a subscriber to the marker");
            sleep(1);
        }
        marker_pub.publish(marker);

        ros::Duration(5).sleep();
      }
      else
      {
        ROS_INFO("the target has reached wrongly!!!");
        return -1;
      }

      current_state = 2;

      break;
    }
    case 2:
    {
      ROS_INFO("current_state : to origin");

      // Move :
      goal.target_pose.pose.position.x = origin[0];
      goal.target_pose.pose.position.y = origin[1];
      goal.target_pose.pose.position.z = origin[2];
      goal.target_pose.pose.orientation.x = origin[3];
      goal.target_pose.pose.orientation.y = origin[4];
      goal.target_pose.pose.orientation.z = origin[5];
      goal.target_pose.pose.orientation.w = origin[6];

      ROS_INFO("goal = {position = {%f %f %f}, orientation = {%f %f %f %f}",
      goal.target_pose.pose.position.x,
      goal.target_pose.pose.position.y,
      goal.target_pose.pose.position.z,
      goal.target_pose.pose.orientation.x,
      goal.target_pose.pose.orientation.y,
      goal.target_pose.pose.orientation.z,
      goal.target_pose.pose.orientation.w);
        
      ac.sendGoal(goal);
      ac.waitForResult();
      ROS_INFO("check the result...");
      if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
      {
        ROS_INFO("the target has reached succesfully!!!");

        // Marker :
        marker.ns = "marker";
        marker.id = 0;
        marker.type = shape;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = origin[0];
        marker.pose.position.y = origin[1];
        marker.pose.position.z = origin[2];
        marker.pose.orientation.x = origin[3];
        marker.pose.orientation.y = origin[4];
        marker.pose.orientation.z = origin[5];
        marker.pose.orientation.w = origin[6];
        marker.scale.x = 0.2;
        marker.scale.y = 0.2;
        marker.scale.z = 0.2;
        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;
        marker.lifetime = ros::Duration();

        while (marker_pub.getNumSubscribers() < 1)
        {
            if (!ros::ok())
            {
            return 0;
            }
            ROS_WARN_ONCE("Please create a subscriber to the marker");
            sleep(1);
        }
        marker_pub.publish(marker);

        ros::Duration(5).sleep();
      }
      else
      {
        ROS_INFO("the target has reached wrongly!!!");
        return -1;
      }

      current_state = 3;

      break;
    }
    case 3:
    
    {
      ROS_INFO("current_state : at origin");

      // Marker :
      marker.ns = "marker";
      marker.id = 0;
      marker.type = shape;
      marker.action = visualization_msgs::Marker::DELETE;
      marker.pose.position.x = origin[0];
      marker.pose.position.y = origin[1];
      marker.pose.position.z = origin[2];
      marker.pose.orientation.x = origin[3];
      marker.pose.orientation.y = origin[4];
      marker.pose.orientation.z = origin[5];
      marker.pose.orientation.w = origin[6];
      marker.scale.x = 0.2;
      marker.scale.y = 0.2;
      marker.scale.z = 0.2;
      marker.color.r = 0.0f;
      marker.color.g = 1.0f;
      marker.color.b = 0.0f;
      marker.color.a = 1.0;
      marker.lifetime = ros::Duration();

      while (marker_pub.getNumSubscribers() < 1)
      {
        if (!ros::ok())
        {
          return 0;
        }
        ROS_WARN_ONCE("Please create a subscriber to the marker");
        sleep(1);
      }
      marker_pub.publish(marker);

      current_state = 0;

      break;
    }
    default:
      current_state = 0;
      break;
    }
  }

  return 0;
}