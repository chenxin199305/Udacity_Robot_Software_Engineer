#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

// Define a client for to send goal requests to the move_base server through a SimpleActionClient
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// Position Information
// 0: from origin to target; 1: at target
// 2: from target to origin; 3: at origin
int robot_current_state = 0;

// Note : 
//    the position is defined by (x, y, z)
//    the orientation is defined by Quartanian (x, y, z, w)
float origin[7] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
float target[7] = {5.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0};

int main(int argc, char** argv){

  ROS_INFO("pick_objects start.");

  // Initialize the simple_navigation_goals node
  ros::init(argc, argv, "simple_navigation_goals");

  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  // Wait 5 sec for move_base action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  while (true)
  {
    move_base_msgs::MoveBaseGoal goal;

    // set up the frame parameters
    goal.target_pose.header.frame_id = "/map";
    goal.target_pose.header.stamp = ros::Time::now();

    ROS_INFO("robot_current_state = %i", robot_current_state);

    switch (robot_current_state)
    {
    case 0:
      ROS_INFO("robot_current_state : go to the target");

      // Define a position and orientation for the robot to reach
      goal.target_pose.pose.position.x = target[0];
      goal.target_pose.pose.position.y = target[1];
      goal.target_pose.pose.position.z = target[2];
      goal.target_pose.pose.orientation.x = target[3];
      goal.target_pose.pose.orientation.y = target[4];
      goal.target_pose.pose.orientation.z = target[5];
      goal.target_pose.pose.orientation.w = target[6];
      break;
    case 1:
      ROS_INFO("robot_current_state : at the target");

      // Define a position and orientation for the robot to reach
      goal.target_pose.pose.position.x = target[0];
      goal.target_pose.pose.position.y = target[1];
      goal.target_pose.pose.position.z = target[2];
      goal.target_pose.pose.orientation.x = target[3];
      goal.target_pose.pose.orientation.y = target[4];
      goal.target_pose.pose.orientation.z = target[5];
      goal.target_pose.pose.orientation.w = target[6];
      break;
    case 2:
      ROS_INFO("robot_current_state : go to the origin");

      // Define a position and orientation for the robot to reach
      goal.target_pose.pose.position.x = origin[0];
      goal.target_pose.pose.position.y = origin[1];
      goal.target_pose.pose.position.z = origin[2];
      goal.target_pose.pose.orientation.x = origin[3];
      goal.target_pose.pose.orientation.y = origin[4];
      goal.target_pose.pose.orientation.z = origin[5];
      goal.target_pose.pose.orientation.w = origin[6];
      break;
    case 3:
      ROS_INFO("robot_current_state : at the origin");

      // Define a position and orientation for the robot to reach
      goal.target_pose.pose.position.x = origin[0];
      goal.target_pose.pose.position.y = origin[1];
      goal.target_pose.pose.position.z = origin[2];
      goal.target_pose.pose.orientation.x = origin[3];
      goal.target_pose.pose.orientation.y = origin[4];
      goal.target_pose.pose.orientation.z = origin[5];
      goal.target_pose.pose.orientation.w = origin[6];
      break;
    default:
      ROS_INFO("robot_current_state : go to the target");
      
      // Define a position and orientation for the robot to reach
      goal.target_pose.pose.position.x = target[0];
      goal.target_pose.pose.position.y = target[1];
      goal.target_pose.pose.position.z = target[2];
      goal.target_pose.pose.orientation.x = target[3];
      goal.target_pose.pose.orientation.y = target[4];
      goal.target_pose.pose.orientation.z = target[5];
      goal.target_pose.pose.orientation.w = target[6];
      break;
    }

    ROS_INFO("goal = {position = {%f %f %f}, orientation = {%f %f %f %f}",
      goal.target_pose.pose.position.x,
      goal.target_pose.pose.position.y,
      goal.target_pose.pose.position.z,
      goal.target_pose.pose.orientation.x,
      goal.target_pose.pose.orientation.y,
      goal.target_pose.pose.orientation.z,
      goal.target_pose.pose.orientation.w);

    // Send the goal position and orientation for the robot to reach
    ROS_INFO("sending goal...");
    ac.sendGoal(goal);

    // Wait an infinite time for the results
    ROS_INFO("wait for result...");
    ac.waitForResult();

    // Check if the robot reached its goal
    ROS_INFO("check the result...");
    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
      ROS_INFO("the target has reached succesfully!!!");
      switch (robot_current_state)
      {
      case 0:
        robot_current_state = 1;
        break;
      case 1:
        robot_current_state = 2;
        break;
      case 2:
        robot_current_state = 3;
        break;    
      case 3:
        robot_current_state = 0;
        break;    
      default:
        robot_current_state = 0;
        break;
      }

      ros::Duration(5).sleep();
    }
    else
    {
      ROS_INFO("the target has reached wrongly!!!");
      return -1;
    }
  }
  
  ROS_INFO("pick_objects end.");
  
  return 0;
}