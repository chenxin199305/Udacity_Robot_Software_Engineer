# Introduction to this workspace folder 
This is the project "Home Service Robot".

The "src" folder contains all the source code.
The "devel" is for setup the PATH.
The "build" is for saving the files for CMake build process.

**Note : The "env_setup.bash" should be "source" before you start the program.**
- It is for setting up the env value TURTLEBOT_GAZEBO_WORLD_FILE, which will be used in the launch file, for find the world file.

**Note : The "devel/setup.bash" should be "source" before you start the program.**
- It is for setting up the env value for the project's packages.

# Introduction to the "workspace - src" folder
## scripts
The "scripts" folder is for storing all the shell files

### launch.sh
"launch.sh" is for complete the "Shell Script" test in this project. It just start the Gazebo and Rviz at the same time, and do nothing else.

### test_slam.sh
"test_slam.sh" is for complete the "SLAM Testing" test in this project. It launchs the:
1. turtlebot_world.launch file to deploy a turtlebot in my environment.
2. gmapping_demo.launch file to run slam_gmapping to perform SLAM.
3. view_navigation.launch to observe the map in rviz.
4. keyboard_teleop.launch to manually control the robot with keyboard commands.

> Problem Met : **However, when I try this slam_testing.sh, I cannot control the robot using the teleop package. I checked the Gazebo and the Rviz, and I find that the Tranform value has error on Rviz. It says it can not find the transform from Base_Link to the Left_Wheel_Link and the Right_Wheel_Link. I think that's why I cannot control these two wheels, but I not sure how to fix it, because the robot model, the urdf file, is predefined. I don't think I should change it? Can you help me with that problem? Thanks.**
> Problem Sloved : By reboot the computer, and use `sudo apt install ros-kinetic-turtlebot-xxx` install related packages, I am not sure why. After the official packages test ok, I rerun my project, it all works well.

### test_navigation.sh
"test_navigation.sh" is for complete the navigation test.

### pick_objects.sh
"pick_objects.sh" is for complete "Localization and Navigation" test. It will control the robot move to the target position, and then go back to the original position.

> Note : 
> - The orientation of the robot goal is defined by Quartanions, we need to find a transfer from Euler Angle to Quartanion to understand it.
> - Find an online tool here : https://quaternions.online/

> Note :
> - The position and orientation is sent based on local axis. Need to pay attention to that.

### home_service_robot.sh
"home_service_robot.sh" is for starting all the nodes related to this project tasks.

Desing Strategy:
- The design of the home_service_robot is using state machine to control the robot state and the pick up state.

## turtlebot_simulator
It is the catkin_package for setup the Gazebo environment.
It uses the world built in the last project, but using the robot turtlebot.
Run `roslaunch turtlebot_gazebo turtlebot_world.launch` to start the Gazebo world.

## turtlebot
It is the catkin_package for teleoperating the robot.
Run `roslaunch turtlebot_teleop keyboard_teleop.launch` to make the teleoperation work.

## slam_gmapping
It is the catkin_package for mapping the virtual world.
Run `roslaunch gmapping slam_gmapping_pr2.launch` to start the mapping algorithm.

## turtlebot_interaction
It is the catkin_package for start the rviz.
Run `roslaunch turtlebot_rviz_launchers view_navigation.launch` to start the rviz world.

## map
It is the catkin_package for store gazebo world file and map generated from SLAM.

## rvizConfig
It is the catkin_package for store customized rviz configuration files.

## pick_objects
It is the catkin_package for drive the robot to do pickup and drop off operations.

## add_markers
It is the catkin_package for write a node to model the object with a marker in rviz.

## home_service
It is the catkin_package for realise the home_service.

