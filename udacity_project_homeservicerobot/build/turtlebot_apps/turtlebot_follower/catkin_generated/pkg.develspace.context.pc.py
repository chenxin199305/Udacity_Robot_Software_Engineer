# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/fftai/Documents/windows_shared_folder/class_udacity_robot_engineer/udacity_robot_software_engineer/udacity_project_homeservicerobot/devel/include".split(';') if "/home/fftai/Documents/windows_shared_folder/class_udacity_robot_engineer/udacity_robot_software_engineer/udacity_project_homeservicerobot/devel/include" != "" else []
PROJECT_CATKIN_DEPENDS = "nodelet;roscpp;visualization_msgs;turtlebot_msgs;depth_image_proc;dynamic_reconfigure".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lturtlebot_follower".split(';') if "-lturtlebot_follower" != "" else []
PROJECT_NAME = "turtlebot_follower"
PROJECT_SPACE_DIR = "/home/fftai/Documents/windows_shared_folder/class_udacity_robot_engineer/udacity_robot_software_engineer/udacity_project_homeservicerobot/devel"
PROJECT_VERSION = "2.3.7"
