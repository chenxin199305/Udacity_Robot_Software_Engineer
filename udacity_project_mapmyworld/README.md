# Introduction to this workspace folder 
This is the project "Map My World".

The "src" folder contains all the source code.
The "devel" is for setup the PATH.
The "build" is for saving the files for CMake build process.

You can using the bash shell file to start all the nodes. But, remember to source the devel/setup.bash at first.
- `source devel/setup.bash`
- `./map_my_world.sh`
  - If the software accidently crash when boot up, please try again.

"rtabmap.db" is the mapping database. It is copied from "~/.ros/rtabmap.db"
- Notice: Remember to run `sudo apt install ros-kinetic-rtabmap-ros` before you run the program.

"Screenshot.png" is to show you that I did successfully run this program on my computer.

# Introduction to the "workspace - src" folder
## my_robot
It is the catkin_package for setup the Gazebo environment, and also the Rviz environment.
It uses the world and the robot built in the last project.
Run "roslaunch my_robot world.launch" to start the Gazebo and the Rviz worlds.

## teleop_twist_keyboard
It is the catkin_package for teleoperating the robot.
Run "rosrun teleop_twist_keyboard teleop_twist_keyboard" to make the teleoperation work.

## map_my_world
It is the catkin_package for mapping the virtual world.
Run "roslaunch map_my_world mapping.launch" to start the RTAB-map algorithm and prepare the database to store the map data.

# Problems Met (just for record, not for project submission, reviewer can ignore it)
> When running the map_my_world package, I get error message :
>     **[ERROR] (2020-02-28 22:45:43.618) OccupancyGrid.cpp:1091::update() Large map size!! map min=(0.254994, -0.405777) max=(1282494093672912722161039310848.000000,29264923023776925352875524096.000000). There's maybe an error with the poses provided! The map will not be created!** Then, I can not have my map saved and no database created. However, I think my map is just 1000 x 1000 pixel size. So, it must not be the map itself, must some configuration problem.
> Problem Solved : By Changing the mapping.launch, add line `<param name="Grid/RangeMax" value="1000"/>`


