#!/bin/sh
xterm -e "roslaunch my_robot world.launch" &
sleep 20

xterm -e "roslaunch map_my_world mapping.launch" &
sleep 10

xterm -e "rosrun teleop_twist_keyboard teleop_twist_keyboard.py" &
sleep 10
